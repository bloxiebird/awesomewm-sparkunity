local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

minimizebutton = wibox.widget {
  image  = beautiful.titlebar_minimize_button_focus,
  resize = true,
  widget = wibox.widget.imagebox
}

minimizebutton:connect_signal("clientbutton::minimize", function()

                        local c = client.focus
                        if c then
                          if c.minimized then
                            c.minimized = false
                          else
                            c.minimized = true
                          end
                        end

end)

minimizebutton:connect_signal("button::press", function()
                        minimizebutton:emit_signal("clientbutton::minimize")
end)

-- client.connect_signal("focus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

-- client.connect_signal("unfocus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

