local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

ontopbutton = wibox.widget {
  image  = beautiful.titlebar_ontop_button_focus_inactive,
  resize = true,
  widget = wibox.widget.imagebox
}

ontopbutton:connect_signal("clientbutton::ontop", function()

                        local c = client.focus
                        if c then
                          if c.ontop then
                            c.ontop = false
                            ontopbutton.image = beautiful.titlebar_ontop_button_focus_inactive
                          else
                            c.ontop = true
                            ontopbutton.image = beautiful.titlebar_ontop_button_focus_active
                          end
                        end

end)

ontopbutton:connect_signal("button::press", function()
                        ontopbutton:emit_signal("clientbutton::ontop")
end)

-- client.connect_signal("focus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

-- client.connect_signal("unfocus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

