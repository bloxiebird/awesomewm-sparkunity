local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

stickybutton = wibox.widget {
  image  = beautiful.titlebar_sticky_button_focus_inactive,
  resize = true,
  widget = wibox.widget.imagebox
}

stickybutton:connect_signal("clientbutton::sticky", function()

                        local c = client.focus
                        if c then
                          if c.sticky then
                            c.sticky = false
                            stickybutton.image  = beautiful.titlebar_sticky_button_focus_inactive
                          else
                            c.sticky = true
                            stickybutton.image  = beautiful.titlebar_sticky_button_focus_active
                          end
                        end

end)

stickybutton:connect_signal("button::press", function()
                        stickybutton:emit_signal("clientbutton::sticky")
end)

-- client.connect_signal("focus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

-- client.connect_signal("unfocus", function()
--                         top_panel:emit_signal("netbook::toggle")
-- end)

