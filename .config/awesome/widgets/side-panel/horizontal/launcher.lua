local awful = require("awful")
local beautiful = require("beautiful")
require("widgets.mainmenu")

mylauncher = awful.widget.launcher({ image = beautiful.dash_launcher_icon,
                                     menu = mymainmenu })
