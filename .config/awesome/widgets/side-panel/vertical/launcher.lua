local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

dashbutton = wibox.widget {
  image  = beautiful.dash_launcher_icon,
  resize = true,
  widget = wibox.widget.imagebox
}

dashbutton:connect_signal("button::press", function()
                            awful.spawn.with_shell("awesome-client 'dashpanel_toggle()' &&" .. apps.dashpanel_launcher .. "&& awesome-client 'dashpanel_toggle()'")
end)

