local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")

local tasklist_buttons = gears.table.join(
  awful.button({ }, 1, function (c)
      if c == client.focus then
        c.minimized = true
      else
        c.first_tag:view_only()
        c:emit_signal(
          "request::activate",
          "tasklist",
          {raise = true}
        )
      end
  end),
  awful.button({ }, 3, function()
      awful.menu.client_list({ theme = { width = 250 } })
  end),
  awful.button({ }, 4, function ()
      awful.client.focus.byidx(1)
  end),
  awful.button({ }, 5, function ()
      awful.client.focus.byidx(-1)
end))

-- Create a tasklist widget
-- This works, however it may be better to inherit the s variable from the top panel itself
awful.screen.connect_for_each_screen(function(s)

    s.mytasklist = awful.widget.tasklist {
      screen   = s,
      -- filter   = awful.widget.tasklist.filter.currenttags,

      -- I changed the filter to all tags, so therefore you can work mouse driven on the current screen if thats what you'd like. You may also consider filter.allscreen to get all tags on every screen.
      filter   = awful.widget.tasklist.filter.alltags,
      buttons  = tasklist_buttons,
      layout   = {
        spacing_widget = {
          {
            forced_width  = 5,
            forced_height = 24,
            thickness     = 1,
            color         = '#777777',
            visible       = false, -- You might want to change this if you want a seperator
            widget        = wibox.widget.separator
          },
          valign = 'center',
          halign = 'center',
          widget = wibox.container.place,
        },
        spacing = 1,
        layout  = wibox.layout.fixed.vertical
      },
      -- Notice that there is *NO* wibox.wibox prefix, it is a template,
      -- not a widget instance.
      widget_template = {
        {
          {
            {
              wibox.widget.base.make_widget(),
              forced_height = 17,
              forced_width  = 2,
              id            = 'background_role',
              widget        = wibox.container.background,
            },
            forced_height = 20,
            forced_width  = 4,
            halign = "right",
            widget = wibox.container.place,
          },
          right = 2,
          widget = wibox.container.margin,
        },
        {
          {
            id     = 'clienticon',
            widget = awful.widget.clienticon,
          },
          margins = 7,
          widget  = wibox.container.margin
        },
        nil,
        create_callback = function(self, c, index, objects) --luacheck: no unused args
          self:get_children_by_id('clienticon')[1].client = c
        end,
        layout = wibox.layout.stack,
      },
    }

end)
