local dpi = require("beautiful.xresources").apply_dpi
local menubar = require("menubar")

apps = {

  -- The dashpanel launcher command
  dashpanel_launcher = "rofi -show drun",

  -- The hudmenu launcher command
  hudmenu_launcher = "python3 ~/.config/awesome/configuration/utils/hud-menu.py",

  -- Your default terminal
  terminal        = "kitty",

  -- Your default text editor
  editor          = os.getenv("EDITOR") or "vim",

  -- editor_cmd = terminal .. " -e " .. editor,

  -- Your default file explorer
  explorer        = "pcmanfm",

  -- Command to lock screen
  lockscreen      = "light-locker-command -l",

}

apps.editor_cmd   = apps.terminal .. " -e " .. apps.editor
apps.explorer_cmd = apps.terminal .. " -e " .. apps.explorer
menubar.utils.terminal = apps.terminal -- Set the terminal for applications that require it


return apps
