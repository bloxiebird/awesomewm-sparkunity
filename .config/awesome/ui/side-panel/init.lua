local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

if configuration.sidepanel_position == "top" or configuration.sidepanel_position == "bottom"  then
  side_panel = require('ui.side-panel.horizontal')
else
  side_panel = require('ui.side-panel.vertical')
end

-- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code

local SidePanelBase = function(s)

  local panel =
    awful.wibar(
      {
        ontop = true,
        screen = s,
        type = "dock",
        position = configuration.sidepanel_position,
        width = configuration.sidepanel_width,
        height = configuration.sidepanel_width,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = true,
        bg = beautiful.side_panel_bg,
        fg = beautiful.side_panel_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )


  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  return panel

end

return SidePanelBase
