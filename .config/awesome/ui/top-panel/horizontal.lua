local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')
require('modules.top-panel.netbook.clickarea')
require('widgets.top-panel.horizontal')

local TopPanel = function(s, top_panel_base)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    wibox(
      {
        ontop = true,
        screen = s,
        type = "dock",
        width = top_panel_base.width,
        height = top_panel_base.height,
        x = top_panel_base.x,
        y = top_panel_base.y,
        bg = "#00000000",
        fg = beautiful.top_panel_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )

  -- panel:struts(
  --   {
  --     top = configuration.toppanel_height
  --   }
  -- )
  --

  panel:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      {
        {
          layout = wibox.layout.fixed.horizontal,
          s.mytaglist,
        },
        left = 4,
        right = 4,
        widget = wibox.container.margin
      },

      {
        {
          layout = wibox.layout.fixed.horizontal,
          clientname,
          id = "clientname",
        },
        halign = "center",
        widget = wibox.container.place
      },

      {
        {
          layout = wibox.layout.fixed.horizontal,
          spacing = 7,
          killbutton,
          maximizebutton,
          minimizebutton,
          stickybutton,
          ontopbutton,
          s.mypromptbox,
          visible = false,
          id = "clientbuttons",
        },
        margins = 4,
        widget = wibox.container.margin
      },
    },
    {
      {
        layout = wibox.layout.fixed.horizontal,
        clickarea, -- Middle Widget
      },
      content_fill_vertical = true,
      content_fill_horizontal = true,
      widget = wibox.container.place
    },
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      -- mykeyboardlayout,
      {
        {
        layout = wibox.layout.fixed.horizontal,
        mysystray,
        },
        left = 6,
        right = 6,
        widget = wibox.container.margin
      },
      -- TODO System tray toggler
      mytextclock,
    },
  }


  return panel
end

return TopPanel

