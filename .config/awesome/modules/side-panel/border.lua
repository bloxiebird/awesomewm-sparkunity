local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')

configuration = require('configuration.config')
-- require('widgets.side-panel.horizontal')

local SidePanelBorder = function(s, side_panel, autohide_trigger)

  -- Wiboxes are much more flexible than wibars simply for the fact that there are no defaults, however if you'd rather have the ease of a wibar you can replace this with the original wibar code
  local panel =
    awful.wibar(
      {
        ontop = true,
        screen = s,
        type = "dock",
        position = configuration.sidepanel_position,
        width = 1,
        height = 1,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = true,
        bg = beautiful.side_panel_border_bg,
        fg = beautiful.side_panel_border_fg,
        -- struts = {
        --   top = configuration.toppanel_height
        -- }
      }
    )


  if type(autohide_trigger) == "function" then
    panel.visible = false
    panel:struts(
      {
        top = 0,
        bottom = 0,
        left = 0,
        right = 0,
      }
    )
  end

  -- panel:setup {
  --   layout = wibox.layout.align.horizontal,
  --   { -- Left widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     mylauncher,
  --   },
  --   s.mytasklist, -- Middle widget
  --   { -- Right widgets
  --     layout = wibox.layout.fixed.horizontal,
  --     s.mylayoutbox,
  --   },
  -- }


  return panel
end

return SidePanelBorder

