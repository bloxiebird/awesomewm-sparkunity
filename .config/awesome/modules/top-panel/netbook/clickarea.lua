local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

clickarea =  wibox.widget {
  opacity = 0,
  widget = wibox.widget.separator
}

