local awful = require('awful')
local beautiful = require('beautiful')
local wibox = require('wibox')
local gears = require('gears')
local naughty = require('naughty')

configuration = require('configuration.config')

require('modules.top-panel.netbook.clickarea')

local NetbookMode = function(s, top_panel, top_panel_base)

  -- This is a really annoying hack to make awesomewm automatically re-update struts by just using assigning struts to a wibox
  -- Is there a method to update struts without doing it like this? Or a method to sanely recheck the size that a maximized client can fit into and apply such geometry? Would be useful!
  -- I hope I can refactor this at some point
  local corrector =
    wibox(
      {
        ontop = false,
        screen = s,
        type = "utility",
        width = 1,
        height = 1,
        visible = true,
        opacity = 0,
        x = s.geometry.x,
        y = s.geometry.y,
        stretch = false,
        bg = beautiful.side_panel_border_bg,
        fg = beautiful.side_panel_border_fg,
        -- struts = {
        --   top = 0,
        --   bottom = 0,
        --   left = 0,
        --   right = 0,
        -- }
      }
    )

  -- Check the top panel and client, whether to get rid of shadows, client titlebars etc.
  function checktop_panel(client)
    if client and client.maximized then
      top_panel_base.type = "dock"

      top_panel:get_children_by_id("clientbuttons")[1].visible = false
      top_panel:get_children_by_id("clientname")[1].visible = true

      awful.titlebar.hide (client)

      -- We're updating the struts, which therefore allows the maximized window to resize itself to fit the new space.
      -- What's going wrong is that I believe the window is being maximized before the titlebar is being removed, therefore leaving a blank spot where the titlebar is supposed to go
      corrector:struts({top = "1", bottom = "1", left = "1", right = "1"})
      corrector:struts({top = "0", bottom = "0", left = "0", right = "0"})

    elseif client and not client.maximized then
      top_panel_base.type = "toolbar"

      top_panel:get_children_by_id("clientbuttons")[1].visible = false
      top_panel:get_children_by_id("clientname")[1].visible = true

      awful.titlebar.show (client)

    else
      -- Else statement just to check if there are no clients on the desktop whatsoever
      top_panel_base.type = "toolbar"

      top_panel:get_children_by_id("clientbuttons")[1].visible = false
      top_panel:get_children_by_id("clientname")[1].visible = true
    end
  end

  -- Client signals to check the state of a client as it is managed
  client.connect_signal("property::maximized", function(c)
                          checktop_panel(c)
  end)

  client.connect_signal("focus", function(c)
                          checktop_panel(c)
  end)

  client.connect_signal("unfocus", function()
                          checktop_panel(nil)
  end)


  -- Toggle the client buttons when a client is maximized
  function clientbuttons_toggle(client)
    if client and client.maximized then

      top_panel:get_children_by_id("clientbuttons")[1].visible = true
      top_panel:get_children_by_id("clientname")[1].visible = false

    elseif client and not client.maximized then

      top_panel:get_children_by_id("clientbuttons")[1].visible = false
      top_panel:get_children_by_id("clientname")[1].visible = true

    else

      -- Else statement just to check if there are no clients on the desktop whatsoever
      top_panel:get_children_by_id("clientbuttons")[1].visible = false
      top_panel:get_children_by_id("clientname")[1].visible = true

    end
  end

  -- Show client buttons on hover over top panel
  top_panel:connect_signal("mouse::enter", function()
                             clientbuttons_toggle(client.focus)
  end)

  top_panel:connect_signal("mouse::leave", function()
                             clientbuttons_toggle(nil)
  end)


  -- Double click the top panel when a client is maximized to unmaximize it
  -- Double click toppanel timer, how long it takes for a 2 clicks to be considered a double click
  function double_click_event_handler(double_click_event)
    if double_click_timer then
      double_click_timer:stop()
      double_click_timer = nil
      return true
    end
    
    double_click_timer = gears.timer.start_new(0.20, function()
                                                 double_click_timer = nil
                                                 return false
    end)
  end

  clickarea:buttons (
    gears.table.join(
      awful.button({ }, 1, function()
          -- WILL EXECUTE THIS ON DOUBLE CLICK
          -- Wrapped in pcall so it suppresses any errors if there are no clients focused
          if pcall(function()
              if double_click_event_handler() and client.focus.maximized then
                client.focus.maximized = not client.focus.maximized
                -- c:raise()
              end

                  end)
          then
            return
          end
      end)

    )
  )

  -- return panel
end

return NetbookMode

